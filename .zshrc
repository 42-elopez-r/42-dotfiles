## Prompt con la ruta actual
#PROMPT='%~ %# '
#
## Historial (casi) infinito
#HISTSIZE=10000000
#SAVEHIST=10000000
#
## Aliases para C
#alias gccw='gcc -Wall -Wextra -Werror'
#alias hitler='norminette -R CheckForbiddenSourceHeader'
#alias jit=git
#
## Variables para plugin de header de C 42
## https://github.com/pbondoer/vim-42header
#export MAIL=elopez-r@student.42madrid.com
#
## Path
#export PATH="$PATH:/Users/elopez-r/.local/bin:/Users/elopez-r/.local/usr/local/bin"
#
## Load Homebrew config script
#source $HOME/.brewconfig.zsh

exec /Users/elopez-r/.brew/bin/bash
#exec /Users/elopez-r/.brew/bin/tmux
