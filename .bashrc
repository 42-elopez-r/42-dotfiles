export SHELL=/bin/bash

source ~/.brewconfig.zsh
export PATH="$PATH:/Users/elopez-r/bin"

alias ls='ls -G'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias cp='cp -v'
alias mv='mv -v'
alias rm='rm -v'
alias ln='ln -v'
alias enej='ps -A | grep -i'
alias tn='trans es:en'
alias te='trans en:es'
alias clw='gcc -Wall -Werror -Wextra'
alias hitler=norminette
alias 💩=norminette
alias hitlernw='norminette | grep -v ^Warning'
alias dff="df -h /Users/elopez-r/"

complete -c man
complete -c which
complete -c type
complete -c whatis

# HSTR configuration - add this to ~/.bashrc
alias hh=hstr                    # hh to be alias for hstr
export HSTR_CONFIG="hicolor,raw-history-view"
shopt -s histappend              # append new history items to .bash_history
export HISTCONTROL=ignorespace   # leading space hides commands from history
export HISTFILESIZE=10000        # increase history file size (default is 500)
export HISTSIZE=${HISTFILESIZE}  # increase history size (default is 500)
# ensure synchronization between Bash memory and history file
export PROMPT_COMMAND="history -a; history -n; ${PROMPT_COMMAND}"
# if this is interactive shell, then bind hstr to Ctrl-r (for Vi mode check doc)
if [[ $- =~ .*i.* ]]; then bind '"\C-r": "\C-a hstr -- \C-j"'; fi
# if this is interactive shell, then bind 'kill last command' to Ctrl-x k
if [[ $- =~ .*i.* ]]; then bind '"\C-xk": "\C-a hstr -k \C-j"'; fi

# Prompt
rojo='\[\033[1;31m\]'
verde='\[\033[1;32m\]'
morado='\[\033[0;35m\]'
azul='\[\033[1;36m\]'
normal='\[\033[0m\]'

PS0='$(enlapsed_time -ia)'
PS1=' ╾<'$azul'\w'$normal'>-<'$morado'$(enlapsed_time comp -ca)'$normal'>╼\n ╾{$(codigo=$? ; if [ $codigo -eq 0 ]; then printf "\[\033[1;32m\]" ; else printf "\[\033[1;31m\]" ; fi ; printf "$codigo\[\033[0m\]")}╼\$ '

pfetch
