source /usr/share/vim/vimrc

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-syntastic/syntastic'
Plugin 'alvan/vim-closetag'
Plugin 'blueshirts/darcula'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tpope/vim-fugitive'
Plugin 'pbondoer/vim-42header'

call vundle#end()            " required
filetype plugin indent on    " required

"set encoding=utf-8
"set tabstop=4 autoindent noexpandtab shiftwidth=4
set number
"set relativenumber
"set background=dark
colorscheme darcula
syntax on

" Resaltado de espacios sobrantes
match Error /\s\+$/

"set cursorline
"highlight CursorLine cterm=inverse

"inoremap " ""<left>
"inoremap ' ''<left>
"inoremap ( ()<left>
"inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

" YouCompleteMe
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_auto_trigger=0
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

"Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1

" Closetag
let g:closetag_filetypes = 'html,xhtml,phtml,php'

"AirLine
let g:airline_theme='deus'
set noshowmode
"let g:airline_powerline_fonts = 1

" Environment variables for the 42 Header
let $USER = "elopez-r"
let $MAIL = "elopez-r@student.42madrid"

" Key to write -> and go to insert mode
nnoremap ç a->

" Map jj in insertion mode to Esc
imap jj <Esc>
